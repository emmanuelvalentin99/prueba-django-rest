from modulo.models import Alumno, Materia
from rest_framework import serializers


class AlumnoSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Alumno
        fields = ['url', 's_nombre', 's_direccion', 'i_telefono']


class MateriaSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Materia
        fields = ['url', 's_nombre']
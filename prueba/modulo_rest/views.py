from modulo.models import Alumno, Materia
from rest_framework import viewsets
from rest_framework import permissions
from modulo_rest.serializers import AlumnoSerializer, MateriaSerializer


class AlumnoViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Alumno.objects.all().order_by('id')
    serializer_class = AlumnoSerializer
    permission_classes = [permissions.IsAuthenticated]


class MateriaViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Materia.objects.all()
    serializer_class = MateriaSerializer
    permission_classes = [permissions.IsAuthenticated]

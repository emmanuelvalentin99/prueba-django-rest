from django.db import models


class Alumno(models.Model):
    s_nombre = models.CharField(max_length=100)
    s_direccion = models.CharField(max_length=200)
    i_telefono = models.IntegerField(default=0)
    def __str__(self):
        return self.s_nombre

class Profesor(models.Model):
    s_nombre = models.CharField(max_length=100)
    s_direccion = models.CharField(max_length=200)
    i_telefono = models.IntegerField(default=0)
    def __str__(self):
        return self.s_nombre

class Materia(models.Model):
    fk_professor = models.ForeignKey(Profesor, on_delete=models.CASCADE)
    s_nombre = models.CharField(max_length=100)
    def __str__(self):
        return self.s_nombre

class  Inscrito(models.Model):
    fk_alumno = models.ForeignKey(Alumno, on_delete=models.CASCADE)
    fk_materia = models.ForeignKey(Materia, on_delete=models.CASCADE)
    s_status =  models.CharField(max_length=20,default="")
    d_fecha = models.DateTimeField('date published')
    def __str__(self):
        return self.s_status
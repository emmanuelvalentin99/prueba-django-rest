from django.shortcuts import render, redirect  
from modulo.forms import AlumnoForm  
from modulo.models import Alumno  
# Create your views here.  
def alu(request):  
    if request.method == "POST":  
        form = AlumnoForm(request.POST)  
        if form.is_valid():  
            try:  
                form.save()  
                return redirect('/show')  
            except:  
                pass  
    else:  
        form = AlumnoForm()  
    return render(request,'index.html',{'form':form})  
def show(request):  
    alumnos = Alumno.objects.all()  
    return render(request,"show.html",{'alumnos':alumnos})  
def edit(request, id):  
    alumno = Alumno.objects.get(pk=id)  
    return render(request,'edit.html', {'alumno':alumno})  
def update(request, id):  
    alumno = Alumno.objects.get(pk=id)  
    form = AlumnoForm(request.POST, instance = alumno)  
    print(form)
    if form.is_valid():  
        form.save()  
        return redirect("/show")  
    return render(request, 'edit.html', {'alumno': alumno})  
def destroy(request, id):  
    alumno = Alumno.objects.get(pk=id)  
    alumno.delete()  
    return redirect("/show")  

from django import forms  
from modulo.models import Alumno  
class AlumnoForm(forms.ModelForm):  
    class Meta:  
        model = Alumno  
        fields = "__all__"